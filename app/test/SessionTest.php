<?php

declare(strict_types=1);

namespace app\test;

use app\common\AppTest;
use app\wander\controller\Session;

class SessionTest extends AppTest
{
    public function testAuth()
    {
        $request = new Request();
        $request->withPost(
            [
                'username' => 'root',
                'password' => '123456',
            ]
        );
        $controller = new Session($this->app);
        $response = $controller->auth($request);
        $resp = $this->request($response);
        dump($resp);
    }
}