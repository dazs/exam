<?php

declare(strict_types=1);

namespace app\control\controller\account;

use app\BaseController;
use app\Request;
use app\common\model\account\Duty as DutyModel;
use think\App;
use think\response\Json;

/**
 * Class Duty
 * @package app\control\controller\account
 */
class Duty extends BaseController
{
    /**
     * @param Request $request
     * @return Json
     *
     * @api {post} /account/duty/search 职务列表
     * @apiGroup AccountDuty
     * @apiName sort1
     * @apiVersion 1.0.0
     *
     * @apiDescription 职务列表
     *
     * @apiParam {number} current=0  页码
     * @apiParam {number} pageSize  页数
     * @apiParam {string} [title]  职务名称
     *
     * @apiSuccess {Number} code    状态码，0：请求成功
     * @apiSuccess {String} message   提示信息
     * @apiSuccess {Object} dataSet    返回数据
     *
     * @apiSuccessExample {json} Success-Response:
     * {"errCode":0,"errMsg":"","dataSet":[],"total":0}
     *
     * @apiErrorExample {json} Error-Response:
     * {"errCode":5001,"errMsg":"接口异常"}
     */
    public function search(Request $request)
    {
        $input = $request->post();
        $pageIndex = empty($input['current']) ? 0 : intval($input['current']);
        $pageSize = empty($input['pageSize']) ? 10 : intval($input['pageSize']);
        $total = 0;
        $filters = [];
        if (!empty($input['title'])) {
            $filters['title'] = $input['title'];
        }
        $dataSet = DutyModel::search($filters, $pageIndex, $pageSize, $total);
        return payload(['dataSet' => $dataSet, 'total' => $total]);
    }

    /**
     * @param Request $request
     * @return Json
     *
     * @api {post} /account/duty/modify 修改职务
     * @apiGroup AccountDuty
     * @apiName sort2
     * @apiVersion 1.0.0
     *
     * @apiDescription 修改职务
     *
     * @apiParam {number} id  id
     * @apiParam {string} title  名称
     *
     * @apiSuccess {Number} code    状态码，0：请求成功
     * @apiSuccess {String} message   提示信息
     * @apiSuccess {Object} dataSet    返回数据
     *
     * @apiSuccessExample {json} Success-Response:
     * {"errCode":0,"errMsg":"","dataSet":[],"total":0}
     *
     * @apiErrorExample {json} Error-Response:
     * {"errCode":5001,"errMsg":"接口异常"}
     */
    public function modify(Request $request)
    {
        $input = $request->post();
        if (empty($input['id']) || empty($input['title'])) {
            return payload(error(-10, '缺少参数'));
        }
        DutyModel::update($input['id'], ['title' => $input['title']]);
        return payload();
    }

    /**
     * @param Request $request
     * @return Json
     *
     * @api {post} /account/duty/add 添加职务
     * @apiGroup AccountDuty
     * @apiName sort3
     * @apiVersion 1.0.0
     *
     * @apiDescription 添加职务
     *
     * @apiParam {string} title  名称
     *
     * @apiSuccess {Number} code    状态码，0：请求成功
     * @apiSuccess {String} message   提示信息
     * @apiSuccess {Object} dataSet    返回数据
     *
     * @apiSuccessExample {json} Success-Response:
     * {"errCode":0,"errMsg":"","dataSet":[],"total":0}
     *
     * @apiErrorExample {json} Error-Response:
     * {"errCode":5001,"errMsg":"接口异常"}
     */
    public function add(Request $request)
    {
        $input = $request->post();
        if (empty($input['title'])) {
            return payload(error(-10, '缺少参数'));
        }
        DutyModel::add(['title' => $input['title']]);
        return payload();
    }

    /**
     * @param Request $request
     * @return Json
     *
     * @api {post} /account/duty/batchAdd 批量添加职务
     * @apiGroup AccountDuty
     * @apiName sort4
     * @apiVersion 1.0.0
     *
     * @apiDescription 批量添加职务
     *
     * @apiParam {Object} title  名称
     *
     * @apiSuccess {Number} code    状态码，0：请求成功
     * @apiSuccess {String} message   提示信息
     * @apiSuccess {Object} dataSet    返回数据
     *
     * @apiSuccessExample {json} Success-Response:
     * {"errCode":0,"errMsg":"","dataSet":[],"total":0}
     *
     * @apiErrorExample {json} Error-Response:
     * {"errCode":5001,"errMsg":"接口异常"}
     */
    public function batchAdd(Request $request)
    {
        $input = $request->post();
        if (empty($input['title']) && is_array($input['title'])) {
            return payload(error(-10, '缺少参数'));
        }
        foreach ($input['title'] as $v) {
            DutyModel::add(['title' => $v]);
        }
        return payload();
    }

    /**
     * @param Request $request
     * @return Json
     *
     * @api {post} /account/duty/delete 删除职务
     * @apiGroup AccountDuty
     * @apiName sort5
     * @apiVersion 1.0.0
     *
     * @apiDescription 删除职务
     *
     * @apiParam {number} [id]  id
     * @apiParam {object} [ids]  id集合
     *
     * @apiSuccess {Number} code    状态码，0：请求成功
     * @apiSuccess {String} message   提示信息
     * @apiSuccess {Object} dataSet    返回数据
     *
     * @apiSuccessExample {json} Success-Response:
     * {"errCode":0,"errMsg":"","dataSet":[],"total":0}
     *
     * @apiErrorExample {json} Error-Response:
     * {"errCode":5001,"errMsg":"接口异常"}
     */
    public function delete(Request $request)
    {
        $input = $request->post();
        if (empty($input['id']) && empty($input['ids'])) {
            return payload(error(-10, '缺少参数'));
        }
        DutyModel::remove($input);
        return payload();
    }

}