<?php

declare(strict_types=1);

namespace app\control\controller\system;

use app\BaseController;
use app\common\model\account\Department;
use app\common\model\account\Duty;
use app\control\model\User;
use think\response\Json;

/**
 * Class Account
 * @package app\control\controller\system
 */
class Account extends BaseController
{
    /**
     * @return Json
     *
     * @api {get} /system/account/current 获取当前用户信息
     * @apiGroup System
     * @apiName sort1
     * @apiVersion 1.0.0
     *
     * @apiDescription 获取当前用户信息
     *
     * @apiSuccess {Number} code    状态码，0：请求成功
     * @apiSuccess {String} message   提示信息
     * @apiSuccess {Object} dataSet    返回数据
     *
     * @apiSuccessExample {json} Success-Response:
     * {"errCode":0,"errMsg":"","dataSet":[]}
     *
     * @apiErrorExample {json} Error-Response:
     * {"errCode":5001,"errMsg":"接口异常"}
     *
     */
    public function current(): Json
    {
        $currentUser = User::fetchCurrent();
        if (!empty($currentUser['department'])) {
            $department = Department::fetch(intval($currentUser['department']));
            if (!empty($department)) {
                $currentUser['departmentTitle'] = $department['title'];
            }
        }
        if (!empty($currentUser['duty'])) {
            $duty = Duty::fetch(intval($currentUser['duty']));
            if (!empty($duty)) {
                $currentUser['dutyTitle'] = $duty['title'];
            }
        }
        unset($currentUser['password'], $currentUser['salt']);
        return payload(['dataSet' => $currentUser]);
    }
}
