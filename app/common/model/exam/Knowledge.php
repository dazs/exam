<?php

declare(strict_types=1);

namespace app\common\model\exam;

use mb\helper\Collection;
use think\facade\Db;
use Exception;
use think\facade\Log;

/**
 * Class Knowledge
 * @package app\common\model\exam
 */
class Knowledge
{
    public const EXPORT_FIELDS = [
        '' => '',
        'knowledgeTitle' => '知识点',
        'subjectTitle' => '科目',
        'num' => '试题数量',
        'score' => '试题总分',
        'point' => '得分率'
    ];
    /**
     * title
     * subjectId
     * @param array $filter
     * @param int $pIndex
     * @param int $pSize
     * @param int $total
     * @return array
     */
    public static function search(array $filter, int $pIndex = 1, int $pSize = 10, &$total = 0)
    {
        $where = [];
        if (!empty($filter['subjectId'])) {
            $where[] = ['subject_id', '=', $filter['subjectId']];
        }
        if (!empty($filter['title'])) {
            $where[] = ['title', 'like', "%{$filter['title']}%"];
        }
        if (!empty($filter['subjectIds'])) {
            $where[] = ['subject_id', 'in', "{$filter['subjectIds']}"];
        }
        try {
            $total = Db::table('exam_knowledge')->where($where)->count();
            $query = Db::table('exam_knowledge')->where($where);
            if (!empty($pIndex)) {
                $query->page($pIndex, $pSize);
            }
            $dataSet = $query->select()->toArray();
            if (!empty($dataSet)) {
                $dataSet = array_map(
                    function ($val) {
                        return Collection::keyStyle($val, Collection::NAME_STYLE_JAVA);
                    },
                    $dataSet
                );
                return $dataSet;
            } else {
                return [];
            }
        } catch (Exception $e) {
            Log::channel('myError')->write($e->getMessage(), \think\Log::ERROR);
        }
        return [];
    }

    /**
     * subjectId
     * title
     * @param array $data
     * @return int|string
     */
    public static function add(array $data)
    {
        $newData = Collection::keyStyle($data, Collection::NAME_STYLE_C);
        $newData = Collection::elements(['subject_id', 'title'], $newData);
        try {
            return Db::table('exam_knowledge')->insertGetId($newData);
        } catch (Exception $e) {
            Log::channel('myError')->write($e->getMessage(), \think\Log::ERROR);
        }
        return 0;
    }

    /**
     * @param array $where
     * @param array $data
     * @return bool
     */
    public static function update(array $where, array $data)
    {
        $newData = Collection::keyStyle($data, Collection::NAME_STYLE_C);
        $newData = Collection::elements(['subject_id', 'title'], $newData);
        try {
            $offect = Db::table('exam_knowledge')
                ->where($where)->update($newData);
            if ($offect === 1) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            Log::channel('myError')->write($e->getMessage(), \think\Log::ERROR);
        }
        return false;
    }

    /**
     * @param string $type
     * @param string $ids
     * @return bool
     */
    public static function delete(string $type, string $ids)
    {
        if ($type == 'single') {
            $where[] = ['id', '=', $ids];
        } else {
            $where[] = ['id', 'in', "{$ids}"];
        }
        try {
            Db::table('exam_knowledge')
                ->where($where)->delete();
            return true;
        } catch (Exception $e) {
            Log::channel('myError')->write($e->getMessage(), \think\Log::ERROR);
        }
        return false;
    }

    /**
     * @param $id
     * @return array
     */
    public static function fetch($id)
    {
        $where = [];
        if (is_array($id)) {
            if (!empty($id['title'])) {
                $where[] = ['title', '=', $id['title']];
            }
        } else {
            $where[] = ['id', '=', $id];
        }
        try {
            $userInfo = Db::table('exam_knowledge')
                ->where($where)
                ->find();
            if (!empty($userInfo)) {
                return Collection::keyStyle($userInfo, Collection::NAME_STYLE_JAVA);
            }
        } catch (Exception $e) {
            Log::channel('myError')->write($e->getMessage(), \think\Log::ERROR);
        }
        return [];
    }

    /**
     * @param string $title
     * @param int $id
     * @return bool
     */
    public static function check(string $title, int $id = 0)
    {
        try {
            $res = Db::table('exam_knowledge')->where(['title' => $title])->find();
            if ((empty($id) && !empty($res)) || (!empty($id) && !empty($res) && ($res['id'] != $id))) {
                return false;
            }
            return true;
        } catch (Exception $e) {
            Log::channel('myError')->write($e->getMessage(), \think\Log::ERROR);
        }
        return false;
    }
}