<?php

declare(strict_types=1);

namespace app\common;

use PHPUnit\Framework\TestCase;
use think\App;
use think\response\Json;

/**
 * Class HttpTest
 * @package app\common
 */
class HttpTest extends TestCase
{
    protected $app;

    protected function setUp(): void
    {
        $this->app = new App();
        $this->app->http->run();
    }

    /**
     * @param Json $response
     * @return mixed
     */
    protected function request(Json $response)
    {
        $content = $response->getContent();
        return json_decode($content, true);
    }
}
