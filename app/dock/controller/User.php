<?php

declare(strict_types=1);

namespace app\dock\controller;

use app\dock\model\User as ModelUser;
use think\response\Json;
use app\BaseController;

/**
 * Class User
 * @package app\dock\controller
 */
class User extends BaseController
{

    public function current(): Json
    {
        $currentUser = ModelUser::fetchCurrent();
        unset($currentUser['password'], $currentUser['salt']);
        return payload(['dataSet' => $currentUser]);
    }
}