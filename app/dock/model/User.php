<?php

declare(strict_types=1);

namespace app\dock\model;

/**
 * Class User
 * @package app\control\controller\model
 */
class User
{
    /**
     * 获取当前用户信息
     * @return array
     */
    public static function fetchCurrent()
    {
        static $currentUser;
        if (empty($currentUser)) {
            $currentUser = session('currentUser');
            if (empty($currentUser)) {
                abort(401, '登录未授权');
            }
            $currentUser = unserialize($currentUser);
        }
        return $currentUser;
    }
}